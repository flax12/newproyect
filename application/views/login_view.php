<?php
   defined('BASEPATH') OR exit('No direct script access allowed');
 ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/styleLogin.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/plugins/jquery.growl/css/jquery.growl.css">

    <title>Programa</title>
  </head>
  <body>
    <input type="hidden" id="base_url" value="<?php echo base_url();?>">

    <div class="container-fluid py-1" id="inicio">
      <p class="titulo1">Proyectos</p>
    </div>

      <div class="col-md-12 my-5 text-center">
          <img src="<?php echo base_url()?>assets/img/logo.jpeg" class="img-fluid">
      </div>

    <div class="container text-center" id="login">

      <form name="formLogin" id="formLogin" >
        <div class="form-group">
          <label for="user"><h2>USUARIO</h2></label>
          <input type="text" class="form-control" id="user" name="user"  required>
        </div>

        <div class="form-group">
          <label for="pass"><h2>NIP</h2></label>
          <input type="password" class="form-control" id="pass" name="pass"  required  maxlength="4">
        </div>

        <div class="col-md-12 mt-5">
          <button type="button" class="btn btn-warning" id="btnLogin">
              <b>CONTINUAR ></b>
          </button>
        </div>
      </form>

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-3.1.1.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/jquery-form-validation/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/jquery.growl/js/jquery.growl.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/login/login.js"></script>
  </body>
</html>