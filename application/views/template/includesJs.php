    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url();?>assets/js/app.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/jquery-form-validation/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/jquery.growl/js/jquery.growl.js"></script>

    <script type="text/javascript" src="<?php echo base_url()?>assets/js/home/ventas.js"></script>