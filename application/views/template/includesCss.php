    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/_all-skins.min.css">
    <link rel="apple-touch-icon" href="<?php echo base_url()?>assets/img/apple-touch-icon.png">
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/img/favicon.ico">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">