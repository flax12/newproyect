<?php $this->load->view("template/header"); ?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

        <section class="content">
          
          <div class="row">
            <div class="col-md-12 pt-3 text-center">
                    <div class="box-header with-border">
                        <h1 class="box-title">Registro Ventas</h1>
                    </div>
            </div>
            
                <form name="formRegistro" id="formRegistro">
                  <div class="form-group col-md-6">
                    <label for="cuenta">Cuenta</label>
                    <input type="text" class="form-control" id="cuenta" name="cuenta" placeholder="Empresa o Alias" required>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="name">Contacto *</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Nombre Completo" required>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="email">Correo</label>
                    <input type="email" class="form-control" id="email" name="email"  placeholder="usuario@dominion.con">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="celular">Movil</label>
                    <input type="text" class="form-control" id="celular" name="celular" placeholder="Número Teléfonico" required>
                  </div>

                  <div class="form-group col-md-12">
                    <select class="form-control" name="llamada" id="llamada">
                      <option value=""><span>LLAMADA</span></option>
                      <option value="1">SI</option>
                      <option value="0">NO</option>
                    </select>
                  </div> 

                  <div class="form-group col-md-12">
                    <label for="descripcion">Descripción</label><br>
                      <textarea class="form-control"  rows="3" name="descripcion" id="descripcion"></textarea>

                  </div>

                  <div class="col-md-12 text-center">
                    <button type="button" class="btn btn-warning " id="btnGuardarVentas">
                       Registrar
                    </button>
                  </div>
                </form>
            

          </div>

        </section>
        
        <!-- Main content 
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                          <h1 class="box-title">Tabla <button class="btn btn-success" onclick="mostrarform(true)"><i class="fa fa-plus-circle"></i> Agregar</button></h1>
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <!-- /.box-header
                    <!-- centro 
                    <div class="panel-body table-responsive" style="height: 400px;" id="listadoregistros">
                        
                    </div>
                    <!--Fin centr
                  </div><!-- /.box 
              </div><!-- /.col
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->


<?php $this->load->view("template/footer"); ?>