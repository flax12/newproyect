<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends CI_Model{

    public function __construct(){
		parent::__construct();
    }

    public function guardarUser($insert){
      $db = $this->db;
      $db->insert("usuario",$insert);
      $insert_id = $db->insert_id();
      return  $insert_id;
    }

    public function ValidarUsuario($usuraio, $pass){
    	$db = $this->db;

    	$db->where('user', $usuraio);
    	$db->where("pass", $pass);

    	$data = $db->get('usuario')->result_array();
    	return $data;

    }


}   


/*End of file Usuario_model.php*/ 
