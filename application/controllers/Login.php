<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("Usuario_model");

        $session = $this->session->all_userdata();

        //print_r($session);

        if(isset($session["id_usuario"]) && $session["id_usuario"] > 0){
            
            redirect("home");
        }

    }

    public function index()
    {
        $this->load->view('login_view');
    }

    public function iniciarSesion(){

        $data = $this->input->post(null,true);

        //var_dump($data);

        if(!isset($data["user"]) || empty($data["user"]) ){
            $response = array(
                "code"    =>    100,
                "mensaje" =>    "usuario invalido"
            );
            echo json_encode($response);
            return;
        }
            
        if(!isset($data["pass"]) || empty($data["pass"])){
            $response = array(
                "code"    =>    200,
                "mensaje" =>    "password inválido"
            );
            echo json_encode($response);
            return;
        }


        $usuario = $data["user"];
        $password = md5($data["pass"]);

        $resultado = $this->Usuario_model->ValidarUsuario($usuario, $password);

        if(count($resultado) > 0){

            $this->session->set_userdata("isLoggedIn", true);
            $this->session->set_userdata("id_usuario", $resultado[0]["id_usuario"]);
            $this->session->set_userdata("nombre", $resultado[0]["user"]);
            $this->session->set_userdata("rol", $resultado[0]["rolUsuario"]);


            //$this->iniciarSesion($resultado[0]["idUsuario"], $ipAddress, $hash);
                
            $response = array(
              "code"    =>  1,
              "mensaje" =>    "Inicio de sesión correcto."
            );
            echo json_encode($response);
            return;
                
        }else{
            $response = array(
              "code"    =>  300,
              "mensaje" =>    "Usuario o password invalido."
            );
            echo json_encode($response);
            return;
        }

    }

}

/*End of file Login.php*/

