<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();	
	}

	public function index()
	{	
		$session = $this->session->all_userdata();

		//print_r($session);

		if(isset($session["id_usuario"]) && $session["id_usuario"] > 0){
			$this->load->view('home_view');
		}else{
			$login = base_url();
        
        	header('Location:' .$login);
		}
			
	}

	public function cerrar_sesion(){
        $this->session->sess_destroy();
        $login = base_url();
        
        header('Location:' .$login);

    }
}

/*End of file Home.php*/