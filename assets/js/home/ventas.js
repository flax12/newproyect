$(function() {

  $("#btnGuardarVentas").click(function(){
    $("#formRegistro").submit();
  });

  ///esto es si le da a la tecla enter
  $(document).keypress(function(e) {
    if(e.which == 13) {
      $("#formRegistro").submit();
    }
  });

  $("#formRegistro").validate({
    rules: {
      cuenta: "required",
      name: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      celular: {
        required: true
      },
      llamada: {
        required: true
      },
      descripcion: {
        required: true
      }
    },
    messages: {
      cuenta: "Campo Oblogatorio",
      name: {
        required : "Campo Oblogatorio"
      },
      email: {
        required : "Campo Oblogatorio",
        email: "Por Favor Ingrese un email Valido"
      },
      celular: {
        required : "Campo Oblogatorio"
      },
      llamada: {
        required : "Campo Oblogatorio"
      },
      descripcion: {
        required : "Campo Oblogatorio"
      }
    }/*,
    submitHandler: function(form) {
      var base_url = $("#base_url").val();
      $.ajax({
        url: base_url + "Login/iniciarSesion",
        type: "POST",
        data: $("#formLogin").serialize(),

        success : function(data){
          var response = $.parseJSON(data);


        }
      });
    }*/
  });
}); 