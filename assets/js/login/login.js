
$(function() {

  $("#btnLogin").click(function(){
    $("#formLogin").submit();
  });

  ///esto es si le da a la tecla enter
  $(document).keypress(function(e) {
    if(e.which == 13) {
      $("#formLogin").submit();
    }
  });

  $("#formLogin").validate({
    rules: {
      user: "required",
      pass: {
        required: true,
        minlength: 4
      }
    },
    messages: {
      user: "Campo Oblogatorio",
      pass: {
        required : "Campo Oblogatorio",
        minlength : "Ingrese mínimo 4 digitos"
      }
    },
    submitHandler: function(form) {
      var base_url = $("#base_url").val();
      $.ajax({
        url: base_url + "Login/iniciarSesion",
        type: "POST",
        data: $("#formLogin").serialize(),

        success : function(data){
          var response = $.parseJSON(data);

                if(response.code == 300){
                    $.growl.error({title: "Hola", message: response.mensaje, duration: 5000 });
                }

                if(response.code == 100){
                    $.growl.error({title: "Hola", message: response.mensaje, duration: 5000 });
                }
                if(response.code == 200){
                    $.growl.error({title: "Hola", message: response.mensaje, duration: 5000 });
                }
                if(response.code == 1){
                    location.reload();
                }

        }
      });
    }
  });
});  